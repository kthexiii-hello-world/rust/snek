# snek

A snake game in rust.

## Table of Contents

- [snek](#snek)
  - [Table of Contents](#table-of-contents)
  - [Requirements](#requirements)
    - [On Windows](#on-windows)
    - [Other platforms](#other-platforms)
  - [How to run](#how-to-run)
  - [Visual Studio Code](#visual-studio-code)

## Requirements
- rust

### On Windows

Go to [rust-lang.org](https://www.rust-lang.org/) and download the rustup installer. You can also install it in another way check the [Other platforms](#other-platforms) section

### Other platforms

You can check out [Other Rust Installation Methods](https://forge.rust-lang.org/infra/other-installation-methods.html) guide on how to install it.

## How to run

You can use this command to run the app

```
cargo run
```

## Visual Studio Code

If you're using Visual Studio Code then you can install the rust extension. 

To debug the application you can create `launch.json` [config](https://code.visualstudio.com/docs/editor/debugging#_launch-configurations), check this [documentaion](https://code.visualstudio.com/docs/editor/debugging) for more info. Together with `tasks.json` you can build and then run the application in vscode. To configure tasks checkout this [documentation](https://code.visualstudio.com/docs/editor/tasks).

You also need to enable **Debug: Allow Breakpoints Everywhere** in the settings. The json id is `debug.allowBreakpointsEverywhere`. As the name suggest it's allowing you to set breakpoints.

Example: `tasks.json`

```json
{
  "version": "2.0.0",
  "tasks": [
    {
      "label": "build",
      "type": "shell",
      "command": "cargo build",
      "problemMatcher": [],
    },
  ]
}
```

The `"build"` value in `"label"` is the name of the task.

Example: `launch.json`

```json
{
  "version": "0.2.0",
  "configurations": [
    {
      "name": "Debug External",
      "type": "cppvsdbg",
      "request": "launch",
      "program": "${workspaceFolder}/target/debug/${workspaceFolderBasename}.exe",
      "args": [],
      "stopAtEntry": false,
      "cwd": "${workspaceFolder}/src",
      "environment": [],
      "externalConsole": true,
      "preLaunchTask": "build",
    },
  ]
}
```

This specific configuration is for windows platform. This runs the executable in a new terminal. 

**Important**: The new opened terminal won't stay open on windows.